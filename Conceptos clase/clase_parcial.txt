Una clase parcial permite dividir la definici�n de una clase en m�ltiples ficheros. 
Despu�s y en tiempo de compilaci�n, los distintos ficheros en los que est� definida la clase se unir�n para formar una sola clase igual que si se hubiera definido inicialmente en un solo fichero.

La magia de combinar distintos ficheros de una misma clase se consigue a trav�s de la palabra clave Partial (en realidad, no hace falta que cada definici�n parcial de la clase est� en un fichero distinto).

Lo cierto es que Visual Studio es bastante flexible con la definici�n de clases parciales y podemos ver cualquiera de los siguientes escenarios:

Un solo fichero con una sola clase con la palabra Partial (es decir, no es parcial pero aunque se indique no hay ning�n error).
Dos ficheros con dos clases, ambas con la palabra clave Partial.
Dos ficheros con dos clases y s�lo una de ellas con la palabra clave Partial.