�Qu� es una clase anidada? Las clases anidadas te permiten agrupar l�gicamente clases que solo se utilizan en un lugar, por lo tanto, esto aumenta el uso de la encapsulaci�n y crea un c�digo m�s f�cil de leer y de mantener.

Una clase anidada no existe independientemente de su clase adjunta. Por lo tanto, el alcance de una clase anidada est� limitado por su clase externa.
Una clase anidada tambi�n es miembro de su clase adjunta. Tambi�n es posible declarar una clase anidada que es local a un bloque.
Como miembro de su clase adjunta, una clase anidada se puede declarar private, public, protected, o default (Leer sobre Modificadores de acceso).
Una clase anidada tiene acceso a los miembros, incluidos los miembros privados, de la clase en la que est� anidado. Sin embargo, lo inverso no es verdadero, es decir, la clase adjunta no tiene acceso a los miembros de la clase anidada.
Hay dos tipos generales de clases anidadas: las que est�n precedidas por el modificador static (static nested class) y las que no lo est�n (inner class).